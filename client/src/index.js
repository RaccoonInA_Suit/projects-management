import React from 'react';
import ReactDOM from 'react-dom';
import {BrowserRouter} from "react-router-dom";
import {Provider} from "react-redux";
import store from "./redux/store";
import App from './App';
import "../src/utils/i18next";

import "./Index.scss";


ReactDOM.render(
  
  <Provider store={store}>
    <BrowserRouter>
      <React.StrictMode>
          <App />
      </React.StrictMode>
    </BrowserRouter>
  </Provider>
  ,document.getElementById('root')
);
