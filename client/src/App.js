import React, { useEffect, useState } from "react";
import AuthForm from "./components/AuthFom/AuthForm";
import LogForm from "./components/LogForm/LogForm";
import Projects from "./components/Projects/Projects";
import Project from "./components/Project/Project";
import UsersOnProject from "./components/UsersOnProject/UsersOnProject";
import TechnologiesOnProject from "./components/TechnologiesOnProject/TechnologiesOnProject";
import EditPage from "./components/EditPage/EditPage";
import Technologies from "./components/Technologies/Technologies";
import Timesheet from "./components/Timesheet/Timesheet";
import { Route, Routes, Link } from "react-router-dom";
import RoleTable from "./components/RoleTable/RoleTable";
import LanguageBtn from "./components/LanguageBtn/LanguageBtn";
import { AuthContext } from "./helpers/AuthContext";
import axios from "axios";
import { useTranslation } from "react-i18next";
import "../src/utils/i18next";
import "bootstrap/dist/css/bootstrap.min.css";
import AccountMenu from "./components/AccountMenu/AccountMenu";
import PageNotFound from "./components/PageNotFound/PageNotFound";
import { useNavigate } from "react-router-dom";
import PassErrorPage from "./components/PassErrorPage/PassErrorPage";
import ProtectedRoute from "./components/ProtectedRoute/ProtectedRoute";

import "./App.scss";

function App() {
  const { t } = useTranslation();
  let navigate = useNavigate();
  const [authState, setAuthState] = useState({
    lastName: "",
    firstName: "",
    id: 0,
    status: false
  });

  useEffect(() => {
    axios.get("http://localhost:3001/auth/auth", {
      headers: {
        accessToken: localStorage.getItem("accessToken"),
      },
    }).then((response) => {
      if (response.data.error) {
        setAuthState({...authState, status: false});
      } else {
        setAuthState({
          lastName: response.data.lastName,
          firstName: response.data.firstName,
          id: response.data.id,
          status: true
        });
      }
    });
  }, []);

  const logout = () => {
    localStorage.removeItem("accessToken");
    setAuthState({
      lastName: "",
      firstName: "",
      id: 0,
      status: false
    });
    navigate("/");
  };
  
  return (
    <>
      <AuthContext.Provider value={{ authState, setAuthState }}>
        <div className="navigation-bar">
          <div className="linkContainer">
            {!authState.status ? (
              <>
                <Link to="/registration">{t("links.link_registration")}</Link>
                <Link to="/">{t("links.link_login")}</Link>
              </>
            ) : (
              <>
                <Link to="/projects">{t("links.link_projects")}</Link>
                <Link to="/timesheet">{t("links.link_timesheet")}</Link>
                <Link to="/technologies">{t("links.link_create_technology")}</Link>
                <div className="accountMenu">
                  <AccountMenu onLogout={logout}/>
                </div>
              </>
            )}
          </div>
          <div className="lang">
            <LanguageBtn/>
          </div>
        </div>          
        <Routes>
          <Route path="/" element={<LogForm />} />
          <Route path="registration" element={<AuthForm />} />
          <Route path="projects" element={
            <ProtectedRoute>
              <Projects/>
            </ProtectedRoute>}/>
          <Route path="timesheet" element={
            <ProtectedRoute>
              <Timesheet />
            </ProtectedRoute>}/>
          <Route path="technologies" element={
            <ProtectedRoute>
              <Technologies />
            </ProtectedRoute>}/>
          <Route path="/projects/:id" element={<EditPage />} >
            <Route path="project" element={<Project />}/>
            <Route path="members" element={<UsersOnProject />}/>
            <Route path="technologies" element={<TechnologiesOnProject />}/>
            <Route path="role" element={<RoleTable/>}/>
          </Route>
          <Route path="unautorized" element={<PassErrorPage/>} />        
          <Route path="*" element={<PageNotFound/>}/>
        </Routes>  
      </AuthContext.Provider>
    </>
  );
}

export default App;
