import axios from "axios";
import store from "../redux/store";

export const getApiCall = async (link, params) => {
  const token = store.getState().authorization;
  const headers = {Authorization: token};
  const response = await axios.get(link, {headers, params});
  
  return response;
};