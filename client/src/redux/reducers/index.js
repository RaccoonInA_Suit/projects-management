import { combineReducers } from "redux";
import { authorizationReducer } from "./authorization";
import { projectReducer } from "./project";
import { projectsReducer } from "./projects";
import { technologiesReducer } from "./technologies";
import { rolesReducer } from "./roles";

const allReducers = combineReducers({
  authorization: authorizationReducer,
  projects: projectsReducer,
  project: projectReducer,
  roles: rolesReducer,
  technologies: technologiesReducer
});

export default allReducers;
