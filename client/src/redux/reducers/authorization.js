import { setAuthorizationToken } from "../const/const";

export const authorizationReducer = (state = "", action) => {
  switch (action.type) {
    case setAuthorizationToken:
      return action.payload;
    default:
      return state;  
  }
};