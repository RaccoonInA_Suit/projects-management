import { setProject } from "../const/const";

export const projectReducer = (state = [], action) => {
  switch (action.type) {
    case setProject:
      return action.payload;
    default:
      return state;  
  }
};
