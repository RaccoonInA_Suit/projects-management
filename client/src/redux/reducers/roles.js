import { setRoles } from "../const/const";

export const rolesReducer = (state = [], action) => {
  switch (action.type) {
    case setRoles:
      return action.payload;
    default:
      return state;  
  }
};
