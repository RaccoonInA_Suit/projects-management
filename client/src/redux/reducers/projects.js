import { setProjects } from "../const/const";

export const projectsReducer = (state = [], action) => {
  switch (action.type) {
    case setProjects:
      return action.payload;
    default:
      return state;  
  }
};
