import { setTechnologies } from "../const/const";

export const technologiesReducer = (state = [], action) => {
  switch (action.type) {
    case setTechnologies:
      return action.payload;
    default:
      return state;  
  }
};
