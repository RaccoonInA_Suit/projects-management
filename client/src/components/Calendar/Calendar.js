import React, { useState } from 'react';
import Calendar from 'react-calendar';
import 'react-calendar/dist/Calendar.css';
import CreateRole from '../CreateRole/CreateRole';

import "./Calendar.scss";

const CalendarPage = () => {
  const [open, setOpen] = useState(false);
  const [date, setDate] = useState(new Date());

  return (
    <div className='app'>
    <CreateRole open={open} setOpen={setOpen} role={" "} />
      <div className='calendar-container'>
        <Calendar
          onChange={setDate}
          onClickDay = {() => setOpen(true)}
          value={date}
        />
      </div>
    </div>
  );
}

export default CalendarPage;