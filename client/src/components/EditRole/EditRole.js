import React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { useTranslation } from "react-i18next";
import "../../utils/i18next";

const EditRole = ({onSave, role, open}) => {
  const [name, setName] = React.useState(role?.name || "");
  const { t } = useTranslation();

  const onSaveClick = () => {
    onSave({name, id: role.id});
  };

  const onCancelClick = () => {
    onSave(null);
  };

  return (
    <Dialog open={open} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">{t("modal.edit_title")}</DialogTitle>
        <DialogContent>
          <TextField  autoFocus margin="dense" name="role" label={role?.name}  fullWidth
                      onChange={(e) => setName(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button onClick={onCancelClick} color="primary">
            {t("modal.cancle")}
          </Button>
          <Button onClick={onSaveClick} color="primary">
            {t("modal.submit")}
          </Button>
        </DialogActions>
    </Dialog>
  );
}

export default EditRole;
