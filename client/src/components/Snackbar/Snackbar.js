import * as React from 'react';
import Snackbar from '@mui/material/Snackbar';
import Alert from '@mui/material/Alert';


const Alerts = ({ message, open, setOpen }) => {

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen({
      open: false
    });
  };

  return (
    <Snackbar open={open.open} autoHideDuration={6000} onClose={handleClose}>
      <Alert onClose={handleClose} severity={open.severity} sx={{ width: '100%' }}>
        {message}
      </Alert>
    </Snackbar>
  );
}

export default Alerts;
