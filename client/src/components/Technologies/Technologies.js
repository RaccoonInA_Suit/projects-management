import React, { useState, useEffect } from 'react';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@material-ui/core/Button';
import EditTechnology from "../EditTechology/EditTechnology";
import BackDrop from "../BackDrop/BackDrop";
import CreateTechnology from '../CreateTechnology/CreateTechnology';
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { setTechnologies } from "../../redux/const/const";
import { actionCreatores } from "../../helpers/actionCreatores";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import Alerts from "../Snackbar/Snackbar";

import "./Technologies.scss";

const Technologies = () => {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(true);
  const [selectedTech, setSelectedTech] = useState(null);
  const [message, setMessage] = useState("");
  const [alert, setAlert] = useState({
    open: false,
    severity: ""
  });
  const dispatch = useDispatch();
  const technologies = useSelector(store => store.technologies);
  const { t } = useTranslation();

  useEffect(() => {
    axios.get(`http://localhost:3001/tech`, {
      headers: {
        accessToken: localStorage.getItem("accessToken"),
      }
    }).then((response) => {
      dispatch(actionCreatores(setTechnologies, response.data));
    })
    .finally(()=> {
      setIsLoading(false);
    });
  }, []);

  const onDelete = (id) => {
    axios.delete(`http://localhost:3001/tech/${id}`).then((response) => {
      if (response.data === 1) {
        const newList = technologies.filter((technology) => {
          return id !== technology.id;
        });
        dispatch(actionCreatores(setTechnologies, newList));
        setMessage(`${t("alerts.technology_delete")}`);
        setAlert({
          open: true,
          severity: "success"
        });
      }
    })
    .catch((error) => {
      setMessage(`${t("alerts.wrong")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
  };

  const onEdit = (tech) => {
    if (tech) {
      const data = {name: tech.name};
      axios.patch(`http://localhost:3001/tech/edit/${tech.id}`, data).then((response) => {
        const newList = technologies.map((tech) => {
          if (response.data.id === tech.id) {
            return response.data;
          }
          setMessage(`${t("alerts.technology_edit")}`);
          setAlert({
            open: true,
            severity: "success"
          });
          return tech;
        })
        dispatch(actionCreatores(setTechnologies, newList));
      })
      .catch((error) => {
        setMessage(`${t("alerts.wrong")}`);
        setAlert({
          open: true,
          severity: "error"
        });
      })
    }
    setSelectedTech(null);
  };

  const onSave = (tech) => {
    const data = {name: tech.name};
    axios.post(`http://localhost:3001/tech`, data).then((response) => {
      const newList = technologies.concat({ name: tech.name });
      dispatch(actionCreatores(setTechnologies, newList));
      setMessage(`${t("alerts.technology_create")}`);
      setAlert({
        open: true,
        severity: "success"
      });
    })
    .catch((error) => {
      setMessage(`${t("alerts.wrong")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
    setOpen(false);
  };

  return (
    <div>
      <EditTechnology open={!!selectedTech} technology={selectedTech} onSave={onEdit} />
      <CreateTechnology open={open} setOpen={setOpen} technology={" "} onSave={onSave} />
      <BackDrop isLoading={isLoading} />
      <button className="addBtn" onClick={() => setOpen(true)} variant="contained">{t("buttons.add_new_btn")}</button>
      <div className="technologiesContainer">
        <div className="body">
          <TableContainer component={Paper}>
            <Table sx={{ minWidth: 720, minHeight: 700}}>
              <TableBody>
                {technologies.map((currentTech) => (
                  <TableRow>
                    <TableCell>{currentTech.name}</TableCell>
                    <TableCell align="right">
                      <Button className="editBtn" onClick={() => setSelectedTech(currentTech)} variant="contained" >{t("buttons.edit_btn")}</Button>
                    </TableCell>
                    <TableCell align="right">
                      <IconButton aria-label="delete" size="large">
                        <DeleteIcon onClick={() => onDelete(currentTech.id)} color="error" />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer> 
        </div>
      </div>
      {
        <Alerts open={alert} setOpen={setAlert} message={message}/>
      } 
    </div>
  );    
};

export default Technologies;
