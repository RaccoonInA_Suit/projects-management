import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import SearchField from './SearchField';
import TimesheetModal from './TimesheetModal';
import axios from 'axios';

import "./Timesheet.scss";

const Timesheet = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [listOfWorkTime, setListOfWorkTime] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:3001/timesheet").then((response) => {
      setListOfWorkTime(response.data);
    });
  }, []);

  const columns = [
    { id: 'projectName', label: 'Project Name', minWidth: 170 },
    { id: 'employeeName', label: 'Employee Name', minWidth: 100, align: 'center' },
    { id: 'monthYear', label: 'month/year', minWidth: 170, align: 'right' }
  ];  

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div>
    {/* <BackDrop isLoading={isLoading} /> */}
    <Paper sx={{ width: '100%', overflow: 'hidden' }}>
      <div className="actionContainer">
        <SearchField/>
        <TimesheetModal/>
      </div>
      <TableContainer sx={{ maxHeight: 722 }}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map((column) => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {listOfWorkTime
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((row) => (
                <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                  <TableCell>{row.projectId}</TableCell>
                  <TableCell align='center'>{row.userId}</TableCell>
                  <TableCell align='right'>{row.date}</TableCell>
                </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        rowsPerPageOptions={[10, 25, 100]}
        component="div"
        count={listOfWorkTime.length}
        rowsPerPage={rowsPerPage}
        page={page}
        onPageChange={handleChangePage}
        onRowsPerPageChange={handleChangeRowsPerPage}
      />
    </Paper>
    </div>
  );
}

export default Timesheet;
