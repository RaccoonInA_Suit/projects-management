import * as React from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import SearchIcon from "@mui/icons-material/Search";

const SearchField = () => {
  return (
    <TextField label="Search..." id="outlined-start-adornment" sx={{ m: 1, width: '40ch' }}
      InputProps={{
        startAdornment: <InputAdornment position="start"><SearchIcon/></InputAdornment>,
      }}
    />
  );
}

export default SearchField;
