import React, { useState, useEffect } from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import TextField from '@mui/material/TextField';
import AdapterDateFns from '@mui/lab/AdapterDateFns';
import LocalizationProvider from '@mui/lab/LocalizationProvider';
import DatePicker from '@mui/lab/DatePicker';
import AddIcon from '@mui/icons-material/Add';
import MonthPicker from '@mui/lab/MonthPicker';
import axios from "axios";

import "./TimesheetModal.scss";

const TimesheetModal = ({onClick, onChange}) => {
  
  const [project, setProject] = useState('');
  const [employee, setEmployee] = useState('');
  const [date, setDate] = useState(null);
  const [listOfProjects, setListOfProjects] = useState([]);
  const [listOfName, setListOfName] = useState([]);
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
      setOpen(prevState => !prevState);
  };

  useEffect(() => {
    axios.get("http://localhost:3001/projects").then((response) => {
      setListOfProjects(response.data);
    });
    axios.get(`http://localhost:3001/auth`).then((response) => {
      setListOfName(response.data);
    });
  }, []);

  const onSave = () => {
    const data = {projectId: project, userId: employee, date: date};
    axios.post(`http://localhost:3001/timesheet`, data).then((response) => {
    });
  };


  // const handleChangeProject = (event) => {
  //   setProject(event.target.value);
  // };

  // const handleChangeEmployee = (event) => {
  //   setEmployee(event.target.value);
  // };

  return (
    <div>
    <Button onClick={handleClickOpen} className="addBtn" sx={{ m: 2}} startIcon={<AddIcon />} variant="outlined">
      Add New Timesheet
    </Button> 
    <Dialog open={open} onClose={handleClickOpen} aria-labelledby="form-dialog-title">
      <DialogTitle id="form-dialog-title">Add new</DialogTitle>
      <DialogContent>
        <div className="selectContainer">
          <FormControl sx={{ minWidth: 250, maxWidth: 300 }}>
            <InputLabel id="demo-simple-select-label">Project</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select" label="Project"
                    onChange={(e) => setProject(e.target.value)}
            >
              {listOfProjects.map((item) => (
                <MenuItem value={item.title}>{item.title}</MenuItem>
              ))}
            </Select>
          </FormControl>

          <FormControl sx={{ minWidth: 250, maxWidth: 300 }}>
            <InputLabel id="demo-simple-select-label">Employee</InputLabel>
            <Select labelId="demo-simple-select-label" id="demo-simple-select" label="Employee"
                    onChange={(e) => setEmployee(e.target.value)}
            >
              {listOfName.map((item) => (
                <MenuItem value={item.firstName}>{item.firstName}</MenuItem>
              ))}
            </Select>
          </FormControl>
        </div>
        <div className="datePickerContainer">
          <LocalizationProvider sx={{ minWidth: 400, maxWidth: 400 }} dateAdapter={AdapterDateFns}>
            <DatePicker label="Month YYYY" value={date} views={['year', 'month']}
                        onChange={(newDate) => {setDate(newDate)}}
                        renderInput={(params) => <TextField {...params} />}
            />
          </LocalizationProvider>
        </div>
      </DialogContent>
      <DialogActions>
        <Button onClick={handleClickOpen} color="primary">
          Cancel
        </Button>
        <Button onClick={onSave} color="primary">
          Submit
        </Button>
      </DialogActions>
    </Dialog>
    </div>
  );
}

export default TimesheetModal;
