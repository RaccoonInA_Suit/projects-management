import React, { useState, useContext } from "react";
import { useNavigate } from "react-router-dom";
import LoadingButton from '@mui/lab/LoadingButton';
import { useTranslation } from "react-i18next";
import axios from "axios";
import "../../utils/i18next";
import Alerts from "../Snackbar/Snackbar";
import { AuthContext } from "../../helpers/AuthContext";
import axios from "axios";

import "./LogForm.scss";

const LogForm = () => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const { setAuthState } = useContext(AuthContext);
  const [alert, setAlert] = useState({
    open: false,
    severity: ""
  });
  const { t } = useTranslation();
  let navigate = useNavigate();

  const login = () => {
    setLoading(true);
    const data = { email: email, password: password };
    axios.post("http://localhost:3001/auth/login", data)
    .then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      } else {
        localStorage.setItem("accessToken", response.data.token);
        setAuthState({
          lastName: response.data.lastName,
          firstName: response.data.firstName,
          id: response.data.id,
          status: true
        });
        navigate("/projects");
      }
    })
    .catch((error) => {
      setMessage(`${t("login.error")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
    .finally(() => {
      setLoading(false);
    });
  };

  const enabled =
    email.length > 0 &&
    password.length > 0;

  return (
    <>
      <div className="loginPage">
        <div className="formContainer">
          <label>{t("login.email")}</label>
          <input onChange={(e) => {setEmail(e.target.value)}} autocomplete="off" id="inputLoginEmail" name="email" placeholder="Email"/>

          <label>{t("login.password")}</label>
          <input onChange={(e) => {setPassword(e.target.value)}} autocomplete="off" id="inputLoginPassword" name="password" placeholder="Password"/>

          <LoadingButton className="loadBtn" disabled={!enabled} onClick={login} type="submit" loading={loading} variant="outlined">
            {t("login.login")}
          </LoadingButton>
        </div>
      </div>
      {
        <Alerts open={alert} setOpen={setAlert} message={message}/>
      }
    </>
  )
}

export default LogForm;
