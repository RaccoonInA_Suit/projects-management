import * as React from 'react';
import InputAdornment from '@mui/material/InputAdornment';
import TextField from '@mui/material/TextField';
import SearchIcon from "@mui/icons-material/Search";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";

const SearchField = () => {
  const { t } = useTranslation();

  return (
    <TextField label={t("search.search")} id="outlined-start-adornment" sx={{ m: 1, width: '40ch' }}
      InputProps={{
        startAdornment: <InputAdornment position="start"><SearchIcon/></InputAdornment>,
      }}
    />
  );
}

export default SearchField;
