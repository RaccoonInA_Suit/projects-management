import React, { useState } from "react";
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@mui/material/TextField';
import AddIcon from '@mui/icons-material/Add';
import axios from "axios";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import Alerts from "../Snackbar/Snackbar";

import "./ProjectModal.scss";

const ProjectModal = ({onClick, onChange}) => {
  const [open, setOpen] = useState(false);
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [message, setMessage] = useState("");
  const { t } = useTranslation();
  const [alert, setAlert] = useState({
    open: false,
    severity: ""
  });

  const handleClickOpen = () => {
    setOpen(prevState => !prevState);
  };

  const onSave = () => {
    const data = {title: title, description: description};
    axios.post(`http://localhost:3001/projects`, data, {
      headers: {
        accessToken: localStorage.getItem("accessToken"),
      }
    })
    .then((response) => {
      setMessage("Projec Create successfuly");
      setAlert({
        open: true,
        severity: "success"
      });
      handleClickOpen();
    })
    .catch((error) => {
      setMessage("Project doesnt create");
      setAlert({
        open: true,
        severity: "error"
      });
    })
    .finally(() => {
    });
  };

  const enabled =
    title.length > 0 &&
    description.length > 0;

  return (
    <>
      <div>
      <Button onClick={handleClickOpen} className="addBtn" sx={{ m: 2}} startIcon={<AddIcon />} variant="outlined">
        {t("projects.create_project")}
      </Button> 
      <Dialog open={open} onClose={handleClickOpen} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">{t("modal.add_title")}</DialogTitle>
        <DialogContent>
          <div className="selectContainer">
            <TextField onChange={(e) => {setTitle(e.target.value)}} className="title" id="outlined-basic" label={t("projects.title")} variant="outlined" />
          </div>
          <div className="datePickerContainer">
            <TextField onChange={(e) => {setDescription(e.target.value)}} className="description" id="outlined-textarea" label={t("projects.description")} placeholder={t("projects.description")} multiline rows={2}/>
          </div>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClickOpen} color="secondary" variant="contained">
            {t("modal.cancle")}
          </Button>
          <Button disabled={!enabled} onClick={onSave} color="primary" variant="contained" >
            {t("modal.submit")}
          </Button>
        </DialogActions>
      </Dialog>
      </div>
      {
        <Alerts open={alert} setOpen={setAlert} message={message}/>
      }
    </>
  );
}

export default ProjectModal;
