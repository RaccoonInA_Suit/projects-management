import React, { useState, useEffect } from 'react';
import Paper from '@mui/material/Paper';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import BackDrop from "../BackDrop/BackDrop";
import SearchField from './SearchField';
import ProjectModal from './ProjectModal';
import { useDispatch, useSelector } from "react-redux";
import { setProjects } from "../../redux/const/const";
import { actionCreatores } from "../../helpers/actionCreatores";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import axios from 'axios';

import "./Projects.scss";

const Projects = () => {
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(25);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { t } = useTranslation();
  const projects = useSelector(store => store.projects);

  useEffect(() => {
    axios.get("http://localhost:3001/projects", {
      headers: {
        accessToken: localStorage.getItem("accessToken"),
      }
    }).then((response) => {
      dispatch(actionCreatores(setProjects, response.data));
    })
    .finally(()=> {
      setIsLoading(false);
    });
  }, []);

  const columns = [
    { id: 'title', label: `${t("projects.title")}`, minWidth: 170 },
    { id: 'description', label: `${t("projects.description")}`, minWidth: 100, align: 'center' },
  ];  

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <div>
      <BackDrop isLoading={isLoading} />
      <Paper sx={{ width: '100%', overflow: 'hidden' }}>
        <div className="actionContainer">
          <SearchField/>
          <ProjectModal/>
        </div>
        <TableContainer sx={{ maxHeight: 722 }}>
          <Table stickyHeader aria-label="sticky table">
            <TableHead className='tableHeader'>
              <TableRow className='tableHeader'>
                {columns.map((column) => (
                  <TableCell className='tableHeader'
                    key={column.id}
                    align={column.align}
                    style={{ minWidth: column.minWidth }}
                  >
                    {column.label}
                  </TableCell>
                ))}
              </TableRow>
            </TableHead>
            <TableBody>
              {projects
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row) => (
                  <TableRow className="tableRow" onClick={() => {navigate(`${row.id}/project`)}} hover role="checkbox" tabIndex={-1} key={row.code}>
                    <TableCell>{row.title}</TableCell>
                    <TableCell align='center'>{row.description}</TableCell>
                  </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[10, 25, 100]}
          component="div"
          count={projects.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onPageChange={handleChangePage}
          onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}

export default Projects;
