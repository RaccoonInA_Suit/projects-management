import React from "react";

import "./PageNotFound.scss";
import notFound from "./pagenotfound.png";

const PageNotFound = () => {

  return (
    <>
      <img className="notFound" src={notFound} alt="Page Not Found"/>
    </>
  )
}

export default PageNotFound;