import React from "react";
import { useNavigate } from "react-router-dom";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";

import "./PassErrorPage.scss";
import notPass from "./nopass.png";

const PassErrorPage = () => {
  const navigate = useNavigate();
  const { t } = useTranslation();

  return (
    <>
      <img className="notPass" src={notPass} alt="You are not loged in"/>
      <div className="contentContainer">
        <h1>{t("error_pages.unauthorized_title")}</h1>
        <p>{t("error_pages.unauthorized_description")}</p>
      </div>
      <button className="redirectBtn" onClick={() => {navigate("/")}} >{t("error_pages.unauthorized_button")}</button>
    </>
  )
}

export default PassErrorPage;
