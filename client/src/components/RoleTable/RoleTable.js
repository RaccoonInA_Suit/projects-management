import React, { useState, useEffect } from 'react';
import { useParams } from "react-router-dom";
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import IconButton from '@mui/material/IconButton';
import DeleteIcon from '@mui/icons-material/Delete';
import Button from '@material-ui/core/Button';
import CreateRole from '../CreateRole/CreateRole';
import BackDrop from "../BackDrop/BackDrop";
import EditRole from "../EditRole/EditRole";
import axios from 'axios';
import { useDispatch, useSelector } from "react-redux";
import { setRoles } from "../../redux/const/const";
import { actionCreatores } from "../../helpers/actionCreatores";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import Alerts from "../Snackbar/Snackbar";

import "./RoleTable.scss";

const RoleTable = () => {
  const { id } = useParams();
  const [isLoading, setIsLoading] = useState(true);
  const [selectedRole, setSelectedRole] = useState(null);
  const [open, setOpen] = useState(false);
  const [message, setMessage] = useState("");
  const [alert, setAlert] = useState({
    open: false,
    severity: ""
  });
  const dispatch = useDispatch();
  const roles = useSelector(store => store.roles);
  const { t } = useTranslation();

  useEffect(() => {
    axios.get(`http://localhost:3001/roles/${id}`).then((response) => {
      dispatch(actionCreatores(setRoles, response.data));
    }).finally(()=> {
      setIsLoading(false);
    });
  }, []);

  const onDelete = (id) => {
    axios.delete(`http://localhost:3001/roles/${id}`).then((response) => {
      if (response.data == 1) {
        const newList = roles.filter((role) => {
        return id !== role.id
      });
        dispatch(actionCreatores(setRoles, newList));
        setMessage(`${t("alerts.role_delete")}`);
        setAlert({
          open: true,
          severity: "success"
        });
      }
    })
    .catch((error) => {
      setMessage(`${t("alerts.wrong")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
  };

  const onEdit = (role) => {
    if (role) {
      const data = {name: role.name};
      axios.patch(`http://localhost:3001/roles/${role.id}`, data).then((response) => {
        const newList = roles.map((role) => {
          if (response.data.id === role.id) {
            return response.data;
          } 
          setMessage(`${t("alerts.role_edit")}`);
          setAlert({
            open: true,
            severity: "success"
          });
          return role
        })
        dispatch(actionCreatores(setRoles, newList));
      })
      .catch((error) => {
        setMessage(`${t("alerts.wrong")}`);
        setAlert({
          open: true,
          severity: "error"
        });
      })
    }
    setSelectedRole(null);
  };

  const onSave = (role) => {
    const data = {name: role.name};
    axios.post(`http://localhost:3001/roles/role/${id}`, data).then((response) => {
      const newList = roles.concat({ name: role.name });
      dispatch(actionCreatores(setRoles, newList));
      setMessage(`${t("alerts.role_create")}`);
      setAlert({
        open: true,
        severity: "success"
      });
    })
    .catch((error) => {
      setMessage(`${t("alerts.wrong")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
    setOpen(false);
  };

  return (
    <div className="tableContainer">
      <CreateRole open={open} setOpen={setOpen} role={" "} onSave={onSave} />
      <EditRole open={!!selectedRole} role={selectedRole} onSave={onEdit} />
      <BackDrop isLoading={isLoading} />
      <div className="body">
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 916, minHeight: 477}}>
            <TableBody>
              {roles.map((currentRole, key) => (
                <TableRow key={key}>
                  <TableCell>{currentRole.name}</TableCell>
                  <TableCell align="right">
                    <Button className="editBtn" onClick={() => setSelectedRole(currentRole)} variant="contained">{t("buttons.edit_btn")}</Button>
                  </TableCell>
                  <TableCell align="right">
                    <IconButton onClick={() => onDelete(currentRole.id)} aria-label="delete" size="large">
                      <DeleteIcon color="error" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              ))}
            </TableBody>
          </Table>
        </TableContainer> 
      </div>
      <div className="footer">
        <Button className="addBtn" onClick={() => setOpen(true)} variant="contained" >{t("buttons.add_new_btn")}</Button>
      </div>
      {
        <Alerts open={alert} setOpen={setAlert} message={message}/>
      }
    </div>
  );
}

export default RoleTable;
