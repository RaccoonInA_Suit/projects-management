import React from 'react';
import { Navigate } from 'react-router-dom';

const ProtectedRoute = ({ children }) => {
  const authed = localStorage.getItem("accessToken");
 
  return authed ? children : <Navigate to="/unautorized"/>
};

export default ProtectedRoute;
