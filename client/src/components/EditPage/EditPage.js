import * as React from 'react';
import Box from '@mui/material/Box';
import Tab from '@mui/material/Tab';
import TabContext from '@mui/lab/TabContext';
import {Link, Outlet} from 'react-router-dom';
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import TabList from '@mui/lab/TabList';

import "./EditPage.scss";

const EditPage = () => {
  const [value, setValue] = React.useState('1');
  const { t } = useTranslation();

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box className="editPageContainer" >
      <TabContext value={value}>
        <Box className="tabsContainer">
          <TabList  onChange={handleChange} textColor="secondary" aria-label="secondary tabs example">
            <Tab className="tab" label={t("project_page.edit_project")} component={Link} value="1" to="project" />
            <Tab className="tab" label={t("project_page.new_role")} component={Link} value="2" to="role" />
            <Tab className="tab" label={t("project_page.change_members")} component={Link} value="3" to="members" />
            <Tab className="tab" label={t("project_page.change_technologies")} component={Link} value="4" to="technologies" />
          </TabList>
        </Box>
        <Outlet/>
      </TabContext>
    </Box>
  );
}

export default EditPage;
