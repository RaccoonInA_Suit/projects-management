import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import axios from "axios";
import BackDrop from "../BackDrop/BackDrop";
import { useDispatch, useSelector } from "react-redux";
import { setProject } from "../../redux/const/const";
import { actionCreatores } from "../../helpers/actionCreatores";
import { useTranslation } from "react-i18next";
import { useNavigate } from "react-router-dom";
import "../../utils/i18next";

import "./Project.scss";
import logotype from "./logotype.png";

const Project = () => { 
  const { id } = useParams();
  const [title, setTitle] = useState("");
  const [description, setDescription] = useState("");
  const [img, setImg] = useState(null);
  const [isLoading, setIsLoading] = useState(true);
  const dispatch = useDispatch();
  const project = useSelector(store => store.project);
  const { t } = useTranslation();
  let navigate = useNavigate();

  const sendFile = () => {
    const data = new FormData();
    data.append("logo", img);
    axios.patch(`http://localhost:3001/projects/single/${id}`, data, {
      headers: {
        "content-type": "multipart/form-data",
      }
    });
  };

  const save = () => {
    const data = {title: title, description: description};
    axios.put(`http://localhost:3001/projects/edit/${id}`, data).then((response) => {
      navigate("/projects");
    });
  };

  useEffect(() => {
    axios.get(`http://localhost:3001/projects/byId/${id}`).then((response) => {
      dispatch(actionCreatores(setProject, response.data));
    }).finally(()=> {
      setIsLoading(false);
    });
  }, []);

  const textToggle = () => {
    if (img == null) {
      return `${t("buttons.select_file")}`;
    } else {
      return `${t("buttons.selected_file")}`;
    }
  };

  return (
    <div>
      <BackDrop isLoading={isLoading} />
      <div className="project" id="individual">
        <div className="titleContainer">
          <div className="logo">
            {
              project.image
                ? <img className="logotype" src={`http://localhost:3001/images/${project.image}`} alt="" />
                : <img className="logotype" src={logotype} alt="logo" />
            }
          </div>
          <div className="title">
            <input className="titleInput" name="title" defaultValue={project.title} onChange={(e) => {setTitle(e.target.value)}}></input>
          </div>
        </div>
        <div className="body">
          <textarea className="textarea" name="description" defaultValue={project.description} onChange={(e) => {setDescription(e.target.value)}}></textarea>
        </div>    
        <div className="footer">
          <div className="uploadBtns">
            <label for="input__file" class="customFileUpload">
              <i></i> {textToggle()}
            </label>
            <input className="upload" id="input__file" type="file" name="logo" onChange={(e) => setImg(e.target.files[0])} />
            <input className="submit" type="submit" value={t("buttons.upload")} onClick={sendFile}/>
          </div>
          <button className="saveBtn" onClick={save}>{t("buttons.save_btn")}</button>
        </div>
      </div>
    </div>

  );
};

export default Project;
