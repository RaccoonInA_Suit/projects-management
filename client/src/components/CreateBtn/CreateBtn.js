import React from "react";
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import "./CreateBtn.scss";

const CreateBtn = ({onClick, onChange}) => {
  
  const [open, setOpen] = React.useState(false);

  const handleClickOpen = () => {
      setOpen(prevState => !prevState);
  };

  return (
    <div>
      <Button variant="outlined" color="primary" onClick={handleClickOpen}>
          Add new role
      </Button>
      <Dialog open={open} onClose={handleClickOpen} aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">Create role</DialogTitle>
          <DialogContent>
            <TextField  autoFocus margin="dense" name="role" label="Role"  fullWidth
                        onChange={onChange}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={handleClickOpen} color="primary">
                Cancel
            </Button>
            <Button onClick={onClick} color="primary">
                Submit
            </Button>
          </DialogActions>
      </Dialog>
    </div>
  );
}

export default CreateBtn;
