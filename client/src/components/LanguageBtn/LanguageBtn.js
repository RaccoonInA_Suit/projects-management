import React from "react";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import LanguageIcon from "@mui/icons-material/Language";
import IconButton from '@mui/material/IconButton';
import { useTranslation } from "react-i18next";
import "../../utils/i18next";

import ru from "./russia.png";
import en from "./united_kingdom.png";
import "./LanguageBtn.scss";

const LanguageBtn = () => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const { i18n } = useTranslation();

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const changeLanguage = (lang) => {
    i18n.changeLanguage(lang);
    handleClose();
  };

  return (
    <div>
      <IconButton onClick={handleClick} aria-label="delete" size="large">
        <LanguageIcon fontSize="inherit" />
      </IconButton>
      <Menu
        id="simple-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
      >
        <MenuItem onClick={() => changeLanguage("ru")}><img className="flag" src={ru} alt="flag"/>Русский</MenuItem>
        <MenuItem onClick={() => changeLanguage("en")}><img className="flag" src={en} alt="flag"/>English</MenuItem>
      </Menu>
    </div>
  );
}

export default LanguageBtn;
