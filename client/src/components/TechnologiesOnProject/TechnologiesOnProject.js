import React, {useState, useEffect} from "react";
import { useParams } from "react-router-dom";
import Button from '@mui/material/Button';
import BackDrop from "../BackDrop/BackDrop";
import axios from "axios";
import { useTranslation } from "react-i18next";
import "../../utils/i18next";

import "./TechnologiesOnProject.scss";

const TechnologiesOnProject = () => {

  const { id } = useParams();
  const [listOfTech, setListOfTech] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const { t } = useTranslation();
  const [boards, setBoards] = useState([
    {id: 1, title: "Free members", items: [...listOfTech]},
    {id: 2, title: "On project", items: []}
  ])

  useEffect(() => {
    setBoards([
      {id: 1, title: `${t("project_tables.free_technologies")}`, items: [...listOfTech]},
      {id: 2, title: `${t("project_tables.on_project")}`, items: []}
    ])
  }, [listOfTech]);

  useEffect(() => {
    axios.get(`http://localhost:3001/tech/techologies/${id}`).then((response) => {
      setListOfTech(response.data);
    }).finally(()=> {
      setIsLoading(false);
    });
  }, []);

  // const onSave = (role) => {
  //   const data = {name: role.name};
  //   axios.post(`http://localhost:3001/roles/role/${id}`, data).then((response) => {
  //     const newList = listOfRole.concat({ name: role.name });
  //     setListOfRole(newList);
  //   });
  //   setOpen(false);
  // };


  const [currentBoard, setCurrentBoard] = useState(null);
  const [currentItem, setCurrentItem] = useState(null);

  const dragOverHandler = (e) => {
    e.preventDefault();
    if (e.target.className == "item") {
      e.target.style.boxShadow = "0 4px 3px gray";
    };
  };

  const dragLeaveHandler = (e) => {
    e.target.style.boxShadow = "none";
  };

  const dragStartHandler = (e, board, item) => {
    setCurrentBoard(board);
    setCurrentItem(item);
  };

  const dragEndHandler = (e) => {
    e.target.style.boxShadow = "none";
  };

  const dropHandler = (e, board, item) => {
    e.preventDefault();
    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);
    const dropIndex = board.items.indexOf(item);
    board.items.splice(dropIndex + 1, 0, currentItem);
    setBoards(boards.map(b => {
      if (b.id === board.id) {
        return board;
      }
      if (b.id === currentBoard.id) {
        return currentBoard;
      }
      return b;
    }));
    e.target.style.boxShadow = "none";
  };

  const dropCardHandler = (e, board) => {
    board.items.push(currentItem);
    const currentIndex = currentBoard.items.indexOf(currentItem);
    currentBoard.items.splice(currentIndex, 1);
    setBoards(boards.map(b => {
      if (b.id === board.id) {
        return board;
      }
      if (b.id === currentBoard.id) {
        return currentBoard;
      }
      return b;
    }))
    e.target.style.boxShadow = "none";
  };

  return (
    <div className="body" >
      <BackDrop isLoading={isLoading} />
      <div className="boardsContainer">
        {boards.map(board => 
          <div className="board"
          onDragOver={(e) => dragOverHandler(e)}
          onDrop={(e) => dropCardHandler(e, board)}>
            <div className="boardTitle">{board.title}</div>
            {board.items.map(item =>
              <div  className="item"
                    draggable={true}
                    onDragOver={(e) => dragOverHandler(e)}
                    onDragLeave={(e) => dragLeaveHandler(e)}
                    onDragStart={(e) => dragStartHandler(e, board, item)}
                    onDragEnd={(e) => dragEndHandler(e)}
                    onDrop={(e) => dropHandler(e, board, item)}
              >{item.name}</div>
            )}
          </div> 
        )}
      </div>
      <div className="footer">
        <button>{t("buttons.save_btn")}</button>     
      </div>
    </div>
  );
};

export default TechnologiesOnProject;
