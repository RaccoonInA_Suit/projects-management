import React, { useState, Fragment, useContext } from "react";
import { useNavigate } from "react-router-dom";
import LoadingButton from '@mui/lab/LoadingButton';
import Tooltip from '@mui/material/Tooltip';
import { Formik, Form, Field, ErrorMessage } from "formik";
import * as Yup from 'yup';
import { useTranslation } from "react-i18next";
import "../../utils/i18next";
import { AuthContext } from "../../helpers/AuthContext";
import Alerts from "../Snackbar/Snackbar";
import axios from "axios";

import "./AuthForm.scss";

const AuthForm = () => {
  const [loading, setLoading] = useState(false);
  const [message, setMessage] = useState("");
  const [alert, setAlert] = useState({
    open: false,
    severity: ""
  });
  const { setAuthState } = useContext(AuthContext);
  const { t } = useTranslation();
  let navigate = useNavigate();
  const initialValues = {
    "firstName": "",
    "lastName": "",
    "email": "",
    "password": ""
  };

  const validationSchema = Yup.object().shape({
    firstName: Yup.string().min(2).max(15, `${t("registartion.validation_first_name")}`).required(`${t("registartion.required")}`),
    lastName: Yup.string().min(3).max(15, `${t("registartion.validation_last_name")}`).required(`${t("registartion.required")}`),
    email: Yup.string().email(`${t("registartion.validation_email")}`).required(`${t("registartion.required")}`),
    password: Yup.string().min(6).max(8).required(`${t("registartion.required")}`).matches(/^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$/,
    `${t("registartion.validation_password")}`),
    confirmPassword: Yup.string().oneOf([Yup.ref("password"), null], `${t("registartion.validation_confirm")}`).required(`${t("registartion.required")}`)
  });

  const onSubmit = (data, { resetForm }) => {
    setLoading(true);
    axios.post("http://localhost:3001/auth", data)
    .then((response) => {
      if (response.data.error) {
        alert(response.data.error);
      } else {
        localStorage.setItem("accessToken", response.data.token);
        setAuthState({
          lastName: response.data.lastName,
          firstName: response.data.firstName,
          id: response.data.id,
          status: true
        });
        navigate("/projects");
      }
      setMessage(`${t("registartion.success")}`);
      setAlert({
        open: true,
        severity: "success"
      });
      navigate("/projects");
    })
    .catch((error) => {
      setMessage(`${t("registartion.error")}`);
      setAlert({
        open: true,
        severity: "error"
      });
    })
    .finally(() => {
      resetForm();
      setLoading(false);
    });
  };

  return (
    <>
      <div className="createUserPage">
        <Formik initialValues={initialValues} onSubmit={onSubmit} validationSchema={validationSchema}>
          {(formik) => (
            <Form className="formContainer">
              <label>{t("registartion.first_name")}</label>
              <Field  data-testid="testInput" autocomplete="off" id="inputCreateFN" name="firstName" placeholder="First Name"/>
              <ErrorMessage name="firstName" component="span" />

              <label>{t("registartion.last_name")}</label>
              <Field  data-testid="testInput" autocomplete="off" id="inputCreateLN" name="lastName" placeholder="Last Name"/>
              <ErrorMessage name="lastName" component="span" />

              <label>{t("registartion.email")}</label>
              <Field  data-testid="testInput" autocomplete="off" id="inputCreateEmail" name="email" placeholder="Email"/>
              <ErrorMessage name="email" component="span" />
              <div className="passcont">
                <div className="passinp">
                  <label>{t("registartion.password")}</label>
                  <Field className="input" data-testid="testInput" autocomplete="off" id="inputCreatePassword" name="password" placeholder="Password"/>
                  <ErrorMessage name="password" component="span" />
                </div>
                <Tooltip placement="bottom-start" title = {
                  <Fragment>
                    <p>• {t("registartion.tooltip_length")}</p>
                    <p>• {t("registartion.tooltip_uppercase")}</p>
                    <p>• {t("registartion.tooltip_lowercase")}</p>
                    <p>• {t("registartion.tooltip_number")}</p>
                    <p>• {t("registartion.tooltip_special")}</p>
                  </Fragment>
                }>
                <p className="policy">{t("registartion.policy")}</p>
                </Tooltip>
              </div>

              <label>{t("registartion.confirm")}</label>
              <Field  data-testid="testInput" autocomplete="off" id="inputCreatePassword" name="confirmPassword" placeholder="Confirm Password"/>
              <ErrorMessage name="confirmPassword" component="span" />

              <LoadingButton data-testid="testButton" disabled={!(formik.isValid && formik.dirty)} type="submit" loading={loading} variant="outlined">
                {t("registartion.registration")}
              </LoadingButton>
            </Form>
          )}
        </Formik>
      </div>
      {
        <Alerts open={alert} setOpen={setAlert} message={message}/>
      }
    </>
  )
}

export default AuthForm;
