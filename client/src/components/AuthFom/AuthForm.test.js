import React, { Suspense } from "react";
import { render, fireEvent } from "@testing-library/react";
import AuthForm from "./AuthForm";

const Fixture = ({children}) => {
  return <Suspense fallback={<div>Loading...</div>}>{children}</Suspense>
}

describe("renderCheck", () => {
  test("checkButton", () => {
    const { getByTestId } = render(
      <Fixture>
        <AuthForm />
      </Fixture>
    );
    const btn = getByTestId("testButton");
    expect(btn).toBeTruthy();
  });
  test("checkInput", () => {
    const { getByTestId } = render(<AuthForm />);
    const input = getByTestId("testInput");
    expect(input).toBeTruthy();
  });
});

describe("changeInput", () => {
  test("onChange", () => {
    const { getByTestId } = render(<AuthForm />);
    const input = getByTestId("testInput");
    fireEvent.change(input, { target: {value: "testValue"} });
  });
});

describe("clickButton", () => {
  test("onSubmit", () => {
    const { getByTestId } = render(<AuthForm />);
    const btn = getByTestId("testButton");
    fireEvent.submit(btn).toHaveBeenCalled();
  });
});

test("checkButton", () => {
  const { getByTestId } = render(<div data-testid="divTest">Hello world</div>);
  const btn = getByTestId("divTest");
  expect(btn).toBeTruthy();
});
