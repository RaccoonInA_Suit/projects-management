const express = require("express");
const mysql = require("mysql");
const cors = require("cors");
const passport = require("passport");
const Strategy = require("passport-http").BasicStrategy;
const path = require("path");
const { FILE_ROUTE } = require("./constants.js");

const app = express();

app.use(express.json());
app.use(cors());
app.use(passport.initialize());
app.use(express.urlencoded({ extended: true }));
app.use(FILE_ROUTE, express.static("images"));

const db = require("./models");

passport.use(new Strategy(
  async function(email, password, done) {
    const user = await db.User.findOne({where: {email: email}});
    if (!user) { return done(null, false); }
    if (!user.validPassword(password)) { return done(null, false); }
    return done(null, user);
  }
));

const projectsRouter = require("./routes/Projects");
const usersRouter = require("./routes/Users");
const userRouter = require("./routes/Session");
const rolesRouter = require("./routes/Roles");
const technologyRouter = require("./routes/Technologies");
const timesheetRouter = require("./routes/Timesheets");

app.use("/projects", projectsRouter);
app.use("/tech", technologyRouter);
app.use("/roles", rolesRouter);
app.use("/auth", usersRouter);
app.use("/timesheet", timesheetRouter);
app.get("/session", userRouter);

app.listen(3001, () => {
  console.log("server start on port 3001");
});
