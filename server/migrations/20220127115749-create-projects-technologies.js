module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable('ProjectsTechnologies', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER
      },
      projectId: {
        type: Sequelize.STRING,
        references: { model: 'Project', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      technologyId: {
        type: Sequelize.STRING,
        references: { model: 'Technology', key: 'id' },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('ProjectsTechnologies');
  }
};