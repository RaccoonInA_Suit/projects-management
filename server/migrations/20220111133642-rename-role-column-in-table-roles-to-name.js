module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn("roles", "role", "name")
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn("roles", "name", "role")
  }
};
