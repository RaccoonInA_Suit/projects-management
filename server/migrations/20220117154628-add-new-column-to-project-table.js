module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.addColumn('projects', 'image', Sequelize.STRING, {
       after: "description"
    });
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.removeColumn("projects", "image");
  }
};
