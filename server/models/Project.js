const {
  Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Project extends Model {

    static associate(models) {
      Project.hasMany(models.Role, { foreignKey: "projectId", as: "roles" });
      Project.belongsToMany(models.User, { foreignKey: "projectId", through: "ProjectsUsers", as: "users"});
      Project.belongsToMany(models.Technology, { foreignKey: "projectId", through: "ProjectsTechnologies", as: "technologies"});
    }
  };
  Project.init({
    title: DataTypes.STRING,
    description: DataTypes.STRING,
    image: DataTypes.STRING
  }, {
    sequelize,
    modelName: "Project",
  });
  return Project;
};
