const {
  Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Role extends Model {

    static associate(models) {
      Role.belongsTo(models.Project, {foreignKey: "projectId", as: "project"});
    }
  };
  Role.init({
    name: DataTypes.STRING,
    projectId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: "Role",
  });
  return Role;
};