const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProjectsUsers extends Model {

    static associate(models) {

    }
  };
  ProjectsUsers.init({
    projectId: DataTypes.STRING,
    userId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProjectsUsers',
  });
  return ProjectsUsers;
};
