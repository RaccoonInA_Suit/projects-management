const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Timesheet extends Model {

    static associate(models) {
    }
  }
  Timesheet.init({
    projectId: DataTypes.STRING,
    userId: DataTypes.STRING,
    date: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Timesheet',
  });
  return Timesheet;
};