const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class ProjectsTechnologies extends Model {

    static associate(models) {
      ProjectsTechnologies.belongsTo(models.Project, {foreignKey: "projectId"});
      ProjectsTechnologies.belongsTo(models.Technology, {foreignKey: "technologyId"});
    }
  };
  ProjectsTechnologies.init({
    projectId: DataTypes.STRING,
    technologyId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'ProjectsTechnologies',
  });
  return ProjectsTechnologies;
};