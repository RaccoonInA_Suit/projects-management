const bcrypt = require("bcrypt");

const {
  Model
} = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class User extends Model {

    static associate(models) {
      User.belongsToMany(models.Project, { foreignKey: "userId", through: "ProjectsUsers", as: "projects"});
    }
  };
  User.init({
    firstName: DataTypes.STRING,
    lastName: DataTypes.STRING,
    email: DataTypes.STRING,
    password: DataTypes.STRING
  }, {
    sequelize,
    modelName: "User",
  });

  User.prototype.validPassword = function (password) {
    return bcrypt.compare(this.password == password);
  };

  return User;
};