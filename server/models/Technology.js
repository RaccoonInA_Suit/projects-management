const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Technology extends Model {

    static associate(models) {
      Technology.belongsToMany(models.Project, { foreignKey: "technologyId", through: "ProjectsTechnologies", as: "projects"});
    }
  }
  Technology.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Technology',
  });
  return Technology;
};
