const multer = require("multer");
const path = require("path");

const fileStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "images");
  },
  filename: (req, file, cb) => {
    const ext = path.extname(file.originalname);
    const filePath = Date.now() + `${ext}`;
    cb(null, filePath);
  }
});

// const types = ["image/png", "image/jpeg", "image/jpg"]

// const imageFilter  = (req, file, cb) => {
//   if(types.includes(file.mimetype)) {
//     cb(null, true)
//   } else {
//     cb(null, false)
//   }
// };

const upload = multer({storage: fileStorage});

module.exports = upload;
