const express = require("express");
const router = express.Router();
const Timesheet = require("../models").Timesheet;

router.post("/", async (req, res) => {
  const timesheet = req.body;
  await Timesheet.create(timesheet);
  res.json(timesheet);
});

router.get("/", async (req, res) => {
  const listOfWorkTime = await Timesheet.findAll();
  res.json(listOfWorkTime);
})

module.exports = router ;