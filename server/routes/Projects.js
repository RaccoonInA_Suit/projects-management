const express = require("express");
const router = express.Router();
const Project = require("../models").Project;
const upload = require("../middleware/file");
const{ getFilePath } = require("../utils/FileUtils");
const { validateToken } = require("../middleware/AuthMiddleware");

router.post("/", validateToken, async (req, res) => {
  const project = req.body;
  await Project.create(project);
  res.json(project);
});

router.get("/", validateToken, async (req, res) => {
  const listOfProjects = await Project.findAll();
  res.json(listOfProjects);
});

router.put("/edit/:id", async (req, res) => {
  const project = await Project.update({title: req.body.title, description: req.body.description}, {where: {id: req.params.id}});
  res.json(project);
});

router.get("/byId/:id", async (req, res) => {
  const id = req.params.id;
  const project = await Project.findByPk(id);
  res.json(project);
});

router.patch("/single/:id", upload.single("logo"), async (req, res) => {
  const id = req.params.id;
  const logo = req.file.filename;
  const project = await Project.findByPk(id);
  await project.update({image: logo});
  const responseData = {...project.get({ plain: true }), image: getFilePath(project.image)};
  res.json(responseData);
});

module.exports = router;
