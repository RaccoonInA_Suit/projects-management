const express = require("express");
const passport = require('passport');
const router = express.Router();

router.get("/session", 
  passport.authenticate('basic', { session: false }),
  function(req, res) {
    res.json(req.user);
});

module.exports = router;