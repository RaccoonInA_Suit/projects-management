const express = require("express");
const router = express.Router();
const Sequelize = require("sequelize");
const Technology = require("../models").Technology;
const Project = require("../models").Project;
const ProjectsTechnologies = require("../models").ProjectsTechnologies;
const { validateToken } = require("../middleware/AuthMiddleware");

router.post("/",validateToken, async (req, res) => {
  const technology = req.body;
  await Technology.create(technology);
  res.json(technology);
});

router.get("/",validateToken, async (req, res) => {
  const listOfTechnologies = await Technology.findAll();
  res.json(listOfTechnologies);
});

router.patch("/edit/:id", async (req, res) => {
  const technology = await Technology.findByPk(req.params.id);
  await technology.update({name: req.body.name});
  res.json(technology);
});

router.delete("/:id", async (req, res) => {
  const technology = await Technology.destroy({
    where: {
      id: req.params.id
    }
  });
  res.json(technology);
});

router.get("/techologies/:projectId", async (req, res) => {
   const { projectId } = req.params;

   const project = await Project.findByPk(projectId, {
     include: {
        association: "technologies",
        as: "projects",
        attributes: ["name"],
        required: true,
        through: {
        attributes: [],
       }
     }
   });
  res.json(project.technologies);
});

router.get("/technnologies/:projectId", async (req, res) => {
  const { projectId } = req.params;
  const project = await Technology.findAll({
    include: [
      { association: "projects", required: false, where: { projectId: projectId } }
    ],
    where: [Sequelize.where(Sequelize.col('ProjectsTechnologies.technologyId'), null)]
  });
  res.json(project.technologies);
});

router.post("/create/:id", async (req, res) => {
  const technology = await ProjectsTechnologies.create(req.body);
  const project = await Project.findOne({
    where: {
      id: req.params.id
    }
  });
  res.json(technology);
});

module.exports = router;
