const express = require("express");
const router = express.Router();
const Role = require("../models").Role;
const Project = require("../models").Project;

router.post("/role/:projectId", async (req, res) => {
  const { projectId } = req.params;
  const { name } = req.body;

  const project = await Project.findByPk(projectId);

  const roles = await Role.create({
    name,
    projectId,
  });
  res.json(roles);
});

router.get("/:projectId", async (req, res) => {
  const listOfRole = await Role.findAll({
    where: {
      projectId: req.params.projectId
    }
  });
  res.json(listOfRole);
});

router.patch("/:id", async (req, res) => {
  const role = await Role.findByPk(req.params.id);
  await role.update({
    name: req.body.name
  });
  res.json(role);
});

router.delete("/:id", async (req, res) => {
  const role = await Role.destroy({
    where: {
      id: req.params.id
    }
  });
  res.json(role);
});

module.exports = router;
