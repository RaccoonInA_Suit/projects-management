const express = require("express");
const { check, validationResult } = require('express-validator/check');
const router = express.Router();
const User = require("../models").User;
const bcrypt = require("bcrypt");
const { sign } = require("jsonwebtoken");
const {validateToken} = require("../middleware/AuthMiddleware");

router.post('/', [
  check('email').isEmail().withMessage('must be not null')
    .custom((value, {req, loc, path}) => {
      return User.findOne({
        where: {
          email: req.body.email,
        }
      }).then(user => {
        if (user) {
          return Promise.reject('this email already exist');
        }
      });
    })
], (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
      return res.status(422).json({errors: errors.array()});
  } else {
    const { firstName, lastName, email, password, id } = req.body;
    bcrypt.hash(password, 10).then((hash) => {
      User.create({
        firstName: firstName,
        lastName: lastName,
        email: email,
        password: hash
      });
      const accessToken = sign({email: email, id: id}, "importantsecret");
      res.json({ token: accessToken, firstName: firstName, lastName: lastName, id: id });
    });
  }
});

router.post("/login", async (req, res) => {
  const { email, password } = req.body;
  const user = await User.findOne({ where: { email: email } });
  if (!user) res.json({ error: "User Doesn't Exist" });
  bcrypt.compare(password, user.password).then((match) => {
    if (!match) res.json({ error: "Wrong Username And Password Combination" });
    const accessToken = sign({email: user.email, id: user.id, firstName: user.firstName, lastName: user.lastName}, "importantsecret");
    res.json({ token: accessToken, firstName: user.firstName, lastName: user.lastName, id: user.id });
  });
});

router.get("/auth", validateToken, (req, res) => {
  res.json(req.user);
});

router.get("/byId/:id", async (req, res) => {
  const id = req.params.id;
  const user = await User.findByPk(id);
  res.json(user);
});

router.get("/", async (req, res) => {
  const listOfName = await User.findAll();
  res.json(listOfName);
});

module.exports = router;
